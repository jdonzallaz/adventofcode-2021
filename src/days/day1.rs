use crate::parsing::parse_lines_unsigned_vec;
use crate::Part;

pub fn day1(data: String, _part: &Part) -> u64 {
    let nbs: Vec<u64> = parse_lines_unsigned_vec(&data);
    let mut count = 0;
    let mut current = u64::MAX;

    match _part {
        Part::One => {
            for nb in nbs {
                if nb > current {
                    count += 1;
                }

                current = nb;
            }
        }
        Part::Two => {
            for i in 0..nbs.len() - 2 {
                let slice = &nbs[i..(i + 3)];
                let sum = slice.iter().sum();
                if sum > current {
                    count += 1;
                }
                current = sum;
            }
        }
    }

    count
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::fs;

    fn get_input() -> String {
        fs::read_to_string("data/day1/input-test.txt").expect("error")
    }

    #[test]
    fn part1_example() {
        assert_eq!(day1(get_input(), &Part::One), 7)
    }
    #[test]
    fn part2_example() {
        assert_eq!(day1(get_input(), &Part::Two), 5)
    }
}
