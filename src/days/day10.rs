use crate::Part;
use std::collections::HashMap;

pub fn day10(_data: String, _part: &Part) -> u64 {
    let error_score_map = HashMap::from([(')', 3), (']', 57), ('}', 1197), ('>', 25137)]);
    let incomplete_score_map = HashMap::from([('(', 1), ('[', 2), ('{', 3), ('<', 4)]);

    let mut error_score = 0;
    let mut incomplete_scores = Vec::new();

    for line in _data.trim().lines() {
        let mut found_error = false;
        let mut stack = Vec::new();
        for c in line.chars() {
            match c {
                '(' | '[' | '{' | '<' => stack.push(c),
                _ => {
                    let last = stack.pop().unwrap();
                    if !((c == ')' && last == '(')
                        || (c == ']' && last == '[')
                        || (c == '}' && last == '{')
                        || (c == '>' && last == '<'))
                    {
                        found_error = true;
                        error_score += error_score_map.get(&c).unwrap();
                        break;
                    }
                }
            }
        }

        if !found_error {
            let mut score = 0;
            while !stack.is_empty() {
                score *= 5;
                score += incomplete_score_map.get(&stack.pop().unwrap()).unwrap();
            }
            incomplete_scores.push(score);
        }
    }

    match _part {
        Part::One => error_score,
        Part::Two => {
            incomplete_scores.sort_unstable();
            incomplete_scores[incomplete_scores.len() / 2]
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::fs;

    fn get_input() -> String {
        fs::read_to_string("data/day10/input-test.txt").expect("error")
    }

    #[test]
    fn part1_example() {
        assert_eq!(day10(get_input(), &Part::One), 26397)
    }
    #[test]
    fn part2_example() {
        assert_eq!(day10(get_input(), &Part::Two), 288957)
    }
}
