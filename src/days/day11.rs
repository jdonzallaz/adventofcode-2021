use crate::Part;
use std::cmp::min;

#[derive(Debug)]
struct Octopus {
    energy: u8,
    has_flashes: bool,
}

fn propagate(os: &mut Vec<Vec<Octopus>>, x_base: usize, y_base: usize) {
    let yfrom = y_base.saturating_sub(1);
    let xfrom = x_base.saturating_sub(1);
    let yto = min(os.len(), y_base + 2);
    let xto = min(os[0].len(), x_base + 2);

    for y in yfrom..yto {
        for x in xfrom..xto {
            if !(x == x_base && y == y_base) {
                os[y][x].energy += 1;
                if !os[y][x].has_flashes && os[y][x].energy > 9 {
                    os[y][x].has_flashes = true;
                    propagate(os, x, y);
                }
            }
        }
    }
}

pub fn day11(_data: String, _part: &Part) -> u64 {
    let mut os: Vec<Vec<Octopus>> = _data
        .trim()
        .lines()
        .map(|line| {
            line.chars()
                .map(|energy| {
                    let energy = energy.to_string().parse().unwrap();
                    Octopus {
                        energy,
                        has_flashes: false,
                    }
                })
                .collect()
        })
        .collect();

    let mut n_flashes = 0;
    let mut i = 0;

    loop {
        // Increment by 1
        for y in 0..os.len() {
            for x in 0..os[0].len() {
                os[y][x].energy += 1;
            }
        }

        // Flash and propagate
        for y in 0..os.len() {
            for x in 0..os[0].len() {
                if !os[y][x].has_flashes && os[y][x].energy > 9 {
                    os[y][x].has_flashes = true;
                    propagate(&mut os, x, y);
                }
            }
        }

        // Count flashes
        let mut n_flash_step = 0;
        for y in 0..os.len() {
            for x in 0..os[0].len() {
                if os[y][x].has_flashes {
                    os[y][x].has_flashes = false;
                    os[y][x].energy = 0;
                    n_flash_step += 1;
                }
            }
        }
        n_flashes += n_flash_step;
        i += 1;

        match _part {
            Part::One => {
                if i == 100 {
                    return n_flashes;
                }
            }
            Part::Two => {
                if n_flash_step == 100 {
                    return i;
                }
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::fs;

    fn get_input() -> String {
        fs::read_to_string("data/day11/input-test.txt").expect("error")
    }

    #[test]
    fn part1_example() {
        assert_eq!(day11(get_input(), &Part::One), 1656)
    }
    #[test]
    fn part2_example() {
        assert_eq!(day11(get_input(), &Part::Two), 195)
    }
}
