use crate::Part;
use std::collections::{HashMap, HashSet};

fn navigate1(current_path: &mut Vec<&str>, routes: &HashMap<&str, HashSet<&str>>) -> u64 {
    let mut n_paths = 0;

    for node in routes.get(current_path.last().unwrap()).unwrap() {
        if *node == "end" {
            n_paths += 1;
        } else if *node == node.to_uppercase() || !current_path.contains(node) {
            let mut new_path = current_path.clone();
            new_path.push(node);
            n_paths += navigate1(&mut new_path, routes);
        }
    }

    n_paths
}

fn navigate2(current_path: &mut Vec<&str>, routes: &HashMap<&str, HashSet<&str>>) -> u64 {
    let mut n_paths = 0;

    let small_caves: Vec<String> = current_path
        .iter()
        .filter(|node| **node == (**node).to_lowercase())
        .map(|n| n.to_string())
        .collect();
    let small_caves_unique: HashSet<_> = small_caves.iter().collect();
    let small_caves_visited_twice = small_caves.len() != small_caves_unique.len();

    for node in routes.get(current_path.last().unwrap()).unwrap() {
        if *node == "end" {
            n_paths += 1;
        } else if *node != "start"
            && (*node == node.to_uppercase()
                || !current_path.contains(node)
                || !small_caves_visited_twice)
        {
            let mut new_path = current_path.clone();
            new_path.push(node);
            n_paths += navigate2(&mut new_path, routes);
        }
    }

    n_paths
}

pub fn day12(_data: String, _part: &Part) -> u64 {
    // Read couple of caves
    let routes_ls: Vec<(&str, &str)> = _data
        .trim()
        .lines()
        .map(|line| line.split_once("-").unwrap())
        .collect();

    // Create map of routes
    let mut routes: HashMap<&str, HashSet<&str>> = HashMap::new();
    for route in routes_ls {
        (*routes.entry(route.0).or_insert_with(HashSet::new)).insert(route.1);
        (*routes.entry(route.1).or_insert_with(HashSet::new)).insert(route.0);
    }

    let mut start: Vec<&str> = vec!["start"];

    match _part {
        Part::One => navigate1(&mut start, &routes),
        Part::Two => navigate2(&mut start, &routes),
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::fs;

    fn get_input() -> String {
        fs::read_to_string("data/day12/input-test.txt").expect("error")
    }
    fn get_input2() -> String {
        fs::read_to_string("data/day12/input-test2.txt").expect("error")
    }
    fn get_input3() -> String {
        fs::read_to_string("data/day12/input-test3.txt").expect("error")
    }

    #[test]
    fn part1_example() {
        assert_eq!(day12(get_input(), &Part::One), 10);
        assert_eq!(day12(get_input2(), &Part::One), 19);
        assert_eq!(day12(get_input3(), &Part::One), 226);
    }
    #[test]
    fn part2_example() {
        assert_eq!(day12(get_input(), &Part::Two), 36);
        assert_eq!(day12(get_input2(), &Part::Two), 103);
        assert_eq!(day12(get_input3(), &Part::Two), 3509);
    }
}
