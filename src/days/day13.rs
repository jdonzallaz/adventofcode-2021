use crate::Part;
use std::cmp::max;
use std::collections::HashSet;

pub fn day13(_data: String, _part: &Part) -> u64 {
    let grid_and_instr: Vec<&str> = _data.trim().split("\n\n").collect();

    // Parse grid of points
    let points = &grid_and_instr[0];
    let mut points: HashSet<(u64, u64)> = points
        .trim()
        .lines()
        .map(|line| line.trim().split_once(",").unwrap())
        .map(|(x, y)| (x.parse().unwrap(), y.parse().unwrap()))
        .collect();

    // Parse instructions
    let instructions = &grid_and_instr[1];
    let instructions: Vec<(char, u64)> = instructions
        .trim()
        .lines()
        .map(|line| {
            line.split_whitespace()
                .last()
                .unwrap()
                .split_once("=")
                .unwrap()
        })
        .map(|(axis, pos)| (axis.parse().unwrap(), pos.parse().unwrap()))
        .collect();

    for step in instructions {
        let (fold_axis, fold_pos) = step;

        let iterator_points = points.clone();
        for point in &iterator_points {
            let (x, y) = *point;

            match fold_axis {
                'x' if x > fold_pos => {
                    points.remove(point);
                    points.insert((2 * fold_pos - x, y));
                }
                'y' if y > fold_pos => {
                    points.remove(point);
                    points.insert((x, 2 * fold_pos - y));
                }
                _ => {}
            }
        }

        match _part {
            Part::One => break,
            Part::Two => {}
        }
    }

    let mut max_x = 0;
    let mut max_y = 0;
    for point in &points {
        let (x, y) = *point;
        max_x = max(x, max_x);
        max_y = max(y, max_y);
    }

    println!("Final grid:");
    for y in 0..max_y + 1 {
        for x in 0..max_x + 1 {
            if points.contains(&(x, y)) {
                print!("x");
            } else {
                print!(" ");
            }
        }
        println!();
    }

    points.len() as u64
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::fs;

    fn get_input() -> String {
        fs::read_to_string("data/day13/input-test.txt").expect("error")
    }

    #[test]
    fn part1_example() {
        assert_eq!(day13(get_input(), &Part::One), 17)
    }
    #[test]
    fn part2_example() {
        assert_eq!(day13(get_input(), &Part::Two), 16)
    }
}
