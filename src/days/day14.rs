use crate::Part;
use std::collections::HashMap;

fn count_chars(polymer: &str) -> Vec<u64> {
    // Return a sorted vec of chars frequencies

    let mut count_map: HashMap<char, u64> = HashMap::new();
    for char in polymer.chars() {
        *count_map.entry(char).or_insert(0) += 1;
    }
    let mut count_arr: Vec<u64> = count_map.values().copied().collect();
    count_arr.sort_unstable();

    count_arr
}

pub fn day14(_data: String, _part: &Part) -> u64 {
    let (polymer, rules) = _data.trim().split_once("\n\n").unwrap();

    let mut polymer = polymer.to_string();

    let rules: HashMap<&str, char> = HashMap::from_iter::<Vec<(&str, char)>>(
        rules
            .trim()
            .lines()
            .map(|line| line.trim().split_once(" -> ").unwrap())
            .map(|(pair, element)| (pair, element.chars().next().unwrap()))
            .collect(),
    );

    if matches!(_part, Part::One) {
        // Apply rules
        for _ in 0..10 {
            for i in (0..polymer.len() - 1).rev() {
                let pair = &polymer[i..i + 2];
                let new_char = *rules.get(pair).unwrap();
                polymer.insert(i + 1, new_char);
            }
        }

        let count_arr = count_chars(&polymer);

        count_arr.last().unwrap() - count_arr[0]
    } else {
        // Create map of pairs, with frequency as value
        let mut pairs: HashMap<String, u64> = polymer
            .chars()
            .collect::<Vec<char>>()
            .windows(2)
            .fold(HashMap::new(), |mut map, pair| {
                *map.entry(String::from_iter(pair)).or_default() += 1;
                map
            });

        // Apply rules
        for _ in 0..40 {
            let pairs_copy = pairs.clone();
            for (pair, &freq) in &pairs_copy {
                let new_char = rules.get(pair.as_str()).unwrap();
                let new_pair1: String = [pair.chars().next().unwrap(), *new_char].iter().collect();
                let new_pair2: String = [*new_char, pair.chars().last().unwrap()].iter().collect();
                *pairs.entry(pair.to_string()).or_default() -= freq;
                *pairs.entry(new_pair1).or_default() += freq;
                *pairs.entry(new_pair2).or_default() += freq;
            }
        }

        // Count chars
        let mut count_map: HashMap<char, u64> = pairs
            .iter()
            .map(|(k, v)| (k.chars().next().unwrap(), v))
            .fold(HashMap::new(), |mut map, (c, freq)| {
                *map.entry(c).or_default() += freq;
                map
            });
        *count_map
            .entry(polymer.chars().last().unwrap())
            .or_default() += 1; // Add last char

        let mut count_arr: Vec<u64> = count_map.values().copied().collect();
        count_arr.sort_unstable();

        count_arr.last().unwrap() - count_arr[0]
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::fs;

    fn get_input() -> String {
        fs::read_to_string("data/day14/input-test.txt").expect("error")
    }

    #[test]
    fn part1_example() {
        assert_eq!(day14(get_input(), &Part::One), 1588)
    }
    #[test]
    fn part2_example() {
        assert_eq!(day14(get_input(), &Part::Two), 2188189693529)
    }
}
