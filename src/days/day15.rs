use crate::parsing::parse_digit_grid;
use crate::Part;
use std::collections::BinaryHeap;

fn shortest_path(map: Vec<Vec<u8>>) -> i128 {
    let goal = (map.len() - 1, map[0].len() - 1);
    let mut risks = vec![vec![i128::MAX; map[0].len()]; map.len()];
    let mut queue = BinaryHeap::from([(0, 0, 0)]);

    while let Some((neg_risk, x, y)) = queue.pop() {
        let risk = -neg_risk;
        if (x, y) == goal {
            return risk;
        } else if risk <= risks[y][x] {
            for (x_test, y_test) in [
                (x.overflowing_sub(1).0, y),
                (x + 1, y),
                (x, y.overflowing_sub(1).0),
                (x, y + 1),
            ] {
                if let Some(risk_test) = map.get(y_test).and_then(|row| row.get(x_test)) {
                    let new_risk = risk + (*risk_test as i128);
                    if new_risk < risks[y_test][x_test] {
                        queue.push((-new_risk, x_test, y_test));
                        risks[y_test][x_test] = new_risk;
                    }
                }
            }
        }
    }
    unreachable!();
}

pub fn day15(_data: String, _part: &Part) -> u64 {
    let mut map = parse_digit_grid(&_data);

    if matches!(_part, Part::Two) {
        map = (0..5 * map.len())
            .map(|y| {
                (0..5 * map[0].len())
                    .map(|x| {
                        let risk = map[y % map.len()][x % map[0].len()]
                            + (y / map.len()) as u8
                            + (x / map[0].len()) as u8;
                        if risk < 10 {
                            risk
                        } else {
                            risk - 9
                        }
                    })
                    .collect()
            })
            .collect();
    }

    let cost = shortest_path(map);

    cost as u64
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::fs;

    fn get_input() -> String {
        fs::read_to_string("data/day15/input-test.txt").expect("error")
    }

    #[test]
    fn part1_example() {
        assert_eq!(day15(get_input(), &Part::One), 40)
    }
    #[test]
    fn part2_example() {
        assert_eq!(day15(get_input(), &Part::Two), 315)
    }
}
