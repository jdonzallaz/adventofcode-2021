use crate::Part;
use std::num::ParseIntError;

type Packet = (u8, u8, PacketData, usize);
enum PacketData {
    Literal(u64),
    Subpackets(Vec<Packet>),
}

fn decode_hex(s: &str) -> Result<Vec<u8>, ParseIntError> {
    s.chars()
        .map(|c| u8::from_str_radix(&c.to_string(), 16))
        .collect()
}

fn encode_binary(d: u8) -> String {
    format!("{:04b}", d)
}

fn decode_binary(s: &str) -> u8 {
    u8::from_str_radix(s, 2).unwrap()
}

fn decode_binary_64(s: &str) -> u64 {
    u64::from_str_radix(s, 2).unwrap()
}

fn hex_to_binary(s: &str) -> String {
    decode_hex(s)
        .unwrap()
        .into_iter()
        .map(encode_binary)
        .collect::<Vec<String>>()
        .join("")
}

fn decode_operator(s: &str) -> (PacketData, usize) {
    let length_type_id = decode_binary(&s[0..1]);
    let mut add_pos = 0;

    let (packets, last_pos) = if length_type_id == 0 {
        let length_packets = decode_binary_64(&s[1..16]);
        add_pos += 16;
        decode_multiple_packets(&s[16..(16 + length_packets as usize)], 0)
    } else {
        let n_packets = decode_binary_64(&s[1..12]);
        add_pos += 12;
        decode_multiple_packets(&s[12..], n_packets)
    };

    (PacketData::Subpackets(packets), last_pos + add_pos)
}

fn decode_literal_value(s: &str) -> (PacketData, usize) {
    let mut binary_str = String::new();
    let mut last_pos = 0;

    for i in (0..s.len()).step_by(5) {
        binary_str.push_str(&s[(i + 1)..(i + 5)]);

        if &s[i..i + 1] == "0" {
            last_pos = i + 5;
            break;
        }
    }

    (
        PacketData::Literal(decode_binary_64(binary_str.as_str())),
        last_pos,
    )
}

fn decode_single_packet(packet: &str) -> Packet {
    let packet_version = decode_binary(&packet[0..3]);
    let packet_type = decode_binary(&packet[3..6]);
    let (data, last_pos) = if packet_type == 4 {
        decode_literal_value(&packet[6..])
    } else {
        decode_operator(&packet[6..])
    };

    (packet_version, packet_type, data, last_pos + 6)
}

fn decode_multiple_packets(packets: &str, n_packets: u64) -> (Vec<Packet>, usize) {
    let mut last_pos = 0;
    let mut processed_packets = Vec::new();

    while (n_packets > 0 && processed_packets.len() < n_packets as usize)
        || (n_packets == 0 && last_pos < packets.len())
    {
        let packet = decode_single_packet(&packets[last_pos..]);
        last_pos += packet.3;
        processed_packets.push(packet);
    }

    (processed_packets, last_pos)
}

fn count_versions(packet: Packet) -> u64 {
    let versions = match packet.2 {
        PacketData::Literal(_) => 0,
        PacketData::Subpackets(ps) => ps.into_iter().map(count_versions).sum(),
    };

    versions + packet.0 as u64
}

fn compute(packet: Packet) -> u64 {
    match packet.2 {
        PacketData::Literal(d) => d,
        PacketData::Subpackets(ps) => {
            let mut values = ps.into_iter().map(compute);

            match packet.1 {
                0 => values.sum(),                                              // sum
                1 => values.product(),                                          // prod
                2 => values.min().unwrap(),                                     // min
                3 => values.max().unwrap(),                                     // max
                5 => (values.next().unwrap() > values.next().unwrap()) as u64,  // >
                6 => (values.next().unwrap() < values.next().unwrap()) as u64,  // <
                7 => (values.next().unwrap() == values.next().unwrap()) as u64, // =
                _ => unreachable!(),
            }
        }
    }
}

pub fn day16(_data: String, _part: &Part) -> u64 {
    let binary = hex_to_binary(_data.trim());
    let packet = decode_single_packet(binary.as_str());

    match _part {
        Part::One => count_versions(packet),
        Part::Two => compute(packet),
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::collections::HashMap;

    #[test]
    fn part1_example() {
        HashMap::from([
            ("8A004A801A8002F478", 16),
            ("620080001611562C8802118E34", 12),
            ("C0015000016115A2E0802F182340", 23),
            ("A0016C880162017C3686B18A3D4780", 31),
        ])
        .iter()
        .for_each(|(&k, &v)| assert_eq!(day16(k.to_string(), &Part::One), v));
    }
    #[test]
    fn part2_example() {
        HashMap::from([
            ("C200B40A82", 3),
            ("04005AC33890", 54),
            ("880086C3E88112", 7),
            ("CE00C43D881120", 9),
            ("D8005AC2A8F0", 1),
            ("F600BC2D8F", 0),
            ("9C005AC2F8F0", 0),
            ("9C0141080250320F1802104A08", 1),
        ])
        .iter()
        .for_each(|(&k, &v)| assert_eq!(day16(k.to_string(), &Part::Two), v));
    }
}
