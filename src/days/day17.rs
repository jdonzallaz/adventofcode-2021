use crate::Part;
use num::integer::sqrt;
use regex::Regex;
use std::cmp::Ordering;

fn hit_target(mut x_speed: i32, mut y_speed: i32, bounds: &[i32]) -> bool {
    let (x_min, x_max, y_min, y_max) = (bounds[0], bounds[1], bounds[2], bounds[3]);
    let mut pos = (0, 0);

    while pos.0 <= x_max && pos.1 >= y_min {
        pos.0 += x_speed;
        pos.1 += y_speed;

        if (x_min..=x_max).contains(&pos.0) && (y_min..=y_max).contains(&pos.1) {
            return true;
        }

        y_speed -= 1;
        x_speed += match x_speed.cmp(&0) {
            Ordering::Less => 1,
            Ordering::Equal => 0,
            Ordering::Greater => -1,
        };
    }

    false
}

pub fn day17(_data: String, _part: &Part) -> u64 {
    let bounds = Regex::new(r"-?\d+")
        .unwrap()
        .find_iter(&_data)
        .map(|mat| mat.as_str())
        .map(|s| s.parse().unwrap())
        .collect::<Vec<i32>>();

    let (x_min, x_max, y_min, _) = (bounds[0], bounds[1], bounds[2], bounds[3]);

    match _part {
        Part::One => {
            let optimal_y_speed = y_min.abs() - 1;
            let max_height = (optimal_y_speed + 1) * optimal_y_speed / 2;
            max_height as u64
        }
        Part::Two => {
            let min_x_speed = (sqrt(8 * x_min + 1) - 1) / 2;
            let max_x_speed = x_max;
            let min_y_speed = y_min;
            let max_y_speed = y_min.abs() - 1;

            let mut count = 0;
            for x_speed in min_x_speed..=max_x_speed {
                for y_speed in min_y_speed..=max_y_speed {
                    if hit_target(x_speed, y_speed, &bounds) {
                        count += 1;
                    }
                }
            }

            count
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::fs;

    fn get_input() -> String {
        fs::read_to_string("data/day17/input-test.txt").expect("error")
    }

    #[test]
    fn part1_example() {
        assert_eq!(day17(get_input(), &Part::One), 45)
    }
    #[test]
    fn part2_example() {
        assert_eq!(day17(get_input(), &Part::Two), 112)
    }
}
