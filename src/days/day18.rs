use crate::Part;
use std::cmp::max;

type Pair = (PairElement, PairElement);

#[derive(Debug, Clone)]
enum PairElement {
    Regular(u8),
    PairEl(Box<Pair>),
}

impl PairElement {
    fn create(el1: Self, el2: Self) -> Self {
        Self::PairEl(Box::new((el1, el2)))
    }
    fn depth(&self) -> usize {
        match self {
            Self::Regular(_) => 0,
            Self::PairEl(pair) => 1 + max((**pair).0.depth(), (**pair).1.depth()),
        }
    }
    fn magnitude(&self) -> usize {
        match self {
            Self::Regular(n) => *n as usize,
            Self::PairEl(pair) => 3 * (**pair).0.magnitude() + 2 * (**pair).1.magnitude(),
        }
    }
    fn add(&self, pair: Self) -> Self {
        Self::PairEl(Box::new((self.clone(), pair)))
    }
    fn reduce(mut self) -> Self {
        loop {
            let depth = self.depth();
            if depth > 4 {
                if let Some((_, element, _)) = self.explode(depth) {
                    self = element;
                    continue;
                }
            }
            match self.split() {
                Some(element) => self = element,
                None => break self,
            }
        }
    }
    fn split(&self) -> Option<Self> {
        match self {
            Self::Regular(x) if *x >= 10 => {
                let a = *x / 2;
                let b = *x - a;
                return Some(Self::create(Self::Regular(a), Self::Regular(b)));
            }
            Self::PairEl(pair) => {
                let e1 = &(**pair).0;
                let e2 = &(**pair).1;
                if let Some(a) = e1.split() {
                    return Some(Self::create(a, e2.clone()));
                }
                if let Some(b) = e2.split() {
                    return Some(Self::create(e1.clone(), b));
                }
            }
            _ => {}
        }
        None
    }

    fn add_left(&self, element: Option<Self>) -> Self {
        match (self, element) {
            (_, None) => self.clone(),
            (Self::Regular(x), Some(Self::Regular(x2))) => Self::Regular(*x + x2),
            (Self::PairEl(pair), element) => {
                let a = &(**pair).0;
                let b = &(**pair).1;
                Self::create(a.add_left(element), b.clone())
            }
            _ => panic!(),
        }
    }

    fn add_right(&self, element: Option<Self>) -> Self {
        match (self, element) {
            (_, None) => self.clone(),
            (Self::Regular(x), Some(Self::Regular(x2))) => Self::Regular(*x + x2),
            (Self::PairEl(pair), element) => {
                let a = &(**pair).0;
                let b = &(**pair).1;
                Self::create(a.clone(), b.add_right(element))
            }
            _ => panic!(),
        }
    }

    fn explode(&self, n: usize) -> Option<(Option<Self>, Self, Option<Self>)> {
        if let Self::PairEl(pair) = self {
            let e1 = &(**pair).0;
            let e2 = &(**pair).1;
            if n == 1 {
                return Some((Some(e1.clone()), Self::Regular(0), Some(e2.clone())));
            }
            if let Some((left, a, right)) = e1.explode(n - 1) {
                let e = Self::create(a, e2.add_left(right));
                return Some((left, e, None));
            }
            if let Some((left, b, right)) = e2.explode(n - 1) {
                let e = Self::create(e1.add_right(left), b);
                return Some((None, e, right));
            }
        }
        None
    }
}

fn parse(s: &[char]) -> (PairElement, usize) {
    let mut pos = 0;

    match s[pos] {
        '[' => {
            pos += 1; // the opening '['
            let (el1, n_read_char) = parse(&s[pos..].to_vec());
            pos += n_read_char;
            pos += 1; // the comma ','
            let (el2, n_read_char) = parse(&s[pos..].to_vec());
            pos += n_read_char;
            pos += 1; // the closing ']'

            (PairElement::PairEl(Box::new((el1, el2))), pos)
        }
        c if c.is_ascii_digit() => {
            let d: u8 = c.to_string().parse().unwrap();
            (PairElement::Regular(d), 1)
        }
        _ => unreachable!(),
    }
}

pub fn day18(_data: String, _part: &Part) -> u64 {
    let pairs: Vec<PairElement> = _data
        .trim()
        .lines()
        .map(|line| parse(&line.chars().collect::<Vec<char>>()).0)
        .collect();

    match _part {
        Part::One => pairs
            .into_iter()
            .reduce(|sum, pair| {
                let tmp_sum = sum.add(pair);
                tmp_sum.reduce()
            })
            .unwrap()
            .magnitude() as u64,
        Part::Two => {
            let mut max_magnitude = 0;
            for i in 0..pairs.len() {
                for j in 0..pairs.len() {
                    if i != j {
                        max_magnitude = max(
                            max_magnitude,
                            pairs[i].add(pairs[j].clone()).reduce().magnitude(),
                        );
                        max_magnitude = max(
                            max_magnitude,
                            pairs[j].add(pairs[i].clone()).reduce().magnitude(),
                        );
                    }
                }
            }
            max_magnitude as u64
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::fs;

    fn get_input() -> String {
        fs::read_to_string("data/day18/input-test.txt").expect("error")
    }

    #[test]
    fn part1_example() {
        assert_eq!(day18(get_input(), &Part::One), 4140);
    }
    #[test]
    fn part2_example() {
        assert_eq!(day18(get_input(), &Part::Two), 3993)
    }
}
