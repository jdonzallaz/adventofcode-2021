use crate::Part;
use itertools::Itertools;
use std::collections::HashSet;
use std::ops::{Add, Sub};

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
struct Point {
    x: i32,
    y: i32,
    z: i32,
}

impl Point {
    fn _origin() -> Self {
        Self { x: 0, y: 0, z: 0 }
    }
    fn _distance(&self, other: &Self) -> i32 {
        // sqrt[(Xa-Xb)²+(Ya-Yb)²+(Za-Zb)²] // ignore sqrt for comparison
        (self.x - other.x).pow(2) + (self.y - other.y).pow(2) + (self.z - other.z).pow(2)
    }
    fn manhattan_distance(&self, other: &Self) -> i32 {
        (self.x - other.x).abs() + (self.y - other.y).abs() + (self.z - other.z).abs()
    }

    fn rotate(&self, i: usize) -> Point {
        let p = [
            [self.x, self.y, self.z],
            [self.x, self.z, -self.y],
            [self.x, -self.y, -self.z],
            [self.x, -self.z, self.y],
            [self.y, self.x, -self.z],
            [self.y, self.z, self.x],
            [self.y, -self.x, self.z],
            [self.y, -self.z, -self.x],
            [self.z, self.x, self.y],
            [self.z, self.y, -self.x],
            [self.z, -self.x, -self.y],
            [self.z, -self.y, self.x],
            [-self.x, self.y, -self.z],
            [-self.x, self.z, self.y],
            [-self.x, -self.y, self.z],
            [-self.x, -self.z, -self.y],
            [-self.y, self.x, self.z],
            [-self.y, self.z, -self.x],
            [-self.y, -self.x, -self.z],
            [-self.y, -self.z, self.x],
            [-self.z, self.x, -self.y],
            [-self.z, self.y, self.x],
            [-self.z, -self.x, self.y],
            [-self.z, -self.y, -self.x],
        ][i];
        Point {
            x: p[0],
            y: p[1],
            z: p[2],
        }
    }
}

impl Sub for Point {
    type Output = Self;

    fn sub(self, other: Self) -> Self::Output {
        Point {
            x: self.x - other.x,
            y: self.y - other.y,
            z: self.z - other.z,
        }
    }
}

impl Add for Point {
    type Output = Self;

    fn add(self, other: Self) -> Self::Output {
        Point {
            x: self.x + other.x,
            y: self.y + other.y,
            z: self.z + other.z,
        }
    }
}

fn parse(data: String) -> Vec<Vec<Point>> {
    data.split("\n\n")
        .map(|scan| {
            scan.lines()
                .skip(1)
                .map(|line| {
                    let mut xs = line.split(',').map(|x| x.parse().unwrap());
                    let x = xs.next().unwrap();
                    let y = xs.next().unwrap();
                    let z = xs.next().unwrap();
                    Point { x, y, z }
                })
                .collect()
        })
        .collect()
}

fn merge_scan(beacons: &mut HashSet<Point>, scan: &[Point]) -> Option<Point> {
    for rotation in 0..24 {
        let rotated_scan = scan.iter().map(|p| p.rotate(rotation)).collect::<Vec<_>>();
        let possible_scanners = beacons
            .iter()
            .cartesian_product(&rotated_scan)
            .map(|(p1, p2)| *p1 - *p2);
        for scanner in possible_scanners {
            let translated = rotated_scan.iter().map(|p| *p + scanner);
            if translated.clone().filter(|v| beacons.contains(v)).count() >= 12 {
                beacons.extend(translated);
                return Some(scanner);
            }
        }
    }
    None
}

pub fn day19(_data: String, _part: &Part) -> u64 {
    let mut scans = parse(_data);
    let mut beacons = scans.remove(0).into_iter().collect::<HashSet<_>>();
    let mut scanners = Vec::new();
    while !scans.is_empty() {
        for i in (0..scans.len()).rev() {
            if let Some(d) = merge_scan(&mut beacons, &scans[i]) {
                scanners.push(d);
                scans.swap_remove(i);
            }
        }
    }

    match _part {
        Part::One => beacons.len() as u64,
        Part::Two => scanners
            .iter()
            .tuple_combinations()
            .map(|(p1, p2)| p1.manhattan_distance(p2))
            .max()
            .unwrap() as u64,
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::fs;

    fn get_input() -> String {
        fs::read_to_string("data/day19/input-test.txt").expect("error")
    }

    #[test]
    fn part1_example() {
        assert_eq!(day19(get_input(), &Part::One), 79)
    }
    #[test]
    fn part2_example() {
        assert_eq!(day19(get_input(), &Part::Two), 3621)
    }
}
