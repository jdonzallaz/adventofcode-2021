use crate::Part;
use std::str::FromStr;

#[derive(Debug)]
enum Direction {
    Down(u64),
    Forward(u64),
    Up(u64),
}

impl FromStr for Direction {
    type Err = ();

    fn from_str(input: &str) -> Result<Direction, Self::Err> {
        let (dir_text, dist_text) = input.split_once(" ").unwrap();
        let dist: u64 = dist_text.parse().unwrap();

        match dir_text {
            "down" => Ok(Direction::Down(dist)),
            "forward" => Ok(Direction::Forward(dist)),
            "up" => Ok(Direction::Up(dist)),
            _ => Err(()),
        }
    }
}
pub fn day2(data: String, part: &Part) -> u64 {
    let mut x = 0;
    let mut y = 0;

    let mut aim = 0;

    for line in data.lines() {
        let dir = Direction::from_str(line).unwrap();

        match part {
            Part::One => match dir {
                Direction::Down(d) => y += d,
                Direction::Forward(d) => x += d,
                Direction::Up(d) => y -= d,
            },
            Part::Two => match dir {
                Direction::Down(d) => aim += d,
                Direction::Up(d) => aim -= d,
                Direction::Forward(d) => {
                    x += d;
                    y += aim * d;
                }
            },
        }
    }

    x * y
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::fs;

    fn get_input() -> String {
        fs::read_to_string("data/day2/input-test.txt").expect("error")
    }

    #[test]
    fn part1_example() {
        assert_eq!(day2(get_input(), &Part::One), 150)
    }
    #[test]
    fn part2_example() {
        assert_eq!(day2(get_input(), &Part::Two), 900)
    }
}
