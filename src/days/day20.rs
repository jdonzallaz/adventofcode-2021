use crate::Part;

fn decode_binary_64(s: &str) -> u64 {
    u64::from_str_radix(s, 2).unwrap()
}

fn add_border(grid: &mut Vec<Vec<bool>>, n: usize, lit: bool) {
    for _ in 0..n {
        grid.insert(0, vec![lit; grid[0].len()]);
        grid.push(vec![lit; grid[0].len()]);
    }

    for row in grid {
        for _ in 0..n {
            row.insert(0, lit);
            row.push(lit);
        }
    }
}

fn switch_border(grid: &mut Vec<Vec<bool>>, n: usize, lit: bool) {
    let grid_len = grid.len();
    let row_len = grid[0].len();
    for i in 0..n {
        grid[i] = vec![lit; row_len];
        grid[grid_len - 1 - i] = vec![lit; row_len];
    }

    for row in grid {
        for i in 0..n {
            row[i] = lit;
            row[row_len - 1 - i] = lit;
        }
    }
}

fn enhance(grid: &mut Vec<Vec<bool>>, algo: &[bool]) -> Vec<Vec<bool>> {
    let mut new_grid = grid.clone();
    for y in 1..grid.len() - 1 {
        for x in 1..grid[0].len() - 1 {
            let mut bin = [false; 9];
            let mut i = 0;
            for py in y - 1..=y + 1 {
                for px in x - 1..=x + 1 {
                    bin[i] = grid[py][px];
                    i += 1;
                }
            }
            let bin = bin
                .iter()
                .map(|b| match b {
                    true => "1".to_string(),
                    false => "0".to_string(),
                })
                .collect::<Vec<String>>()
                .join("");
            let bin = decode_binary_64(&bin) as usize;
            let new_pixel = algo[bin];
            new_grid[y][x] = new_pixel;
        }
    }
    new_grid
}

fn _print_grid(grid: &[Vec<bool>]) {
    for row in grid {
        for lit in row {
            match lit {
                true => print!("#"),
                false => print!("."),
            }
        }
        println!();
    }
    println!();
}

pub fn day20(_data: String, _part: &Part) -> u64 {
    let (algo, grid) = _data.trim().split_once("\n\n").unwrap();

    let algo: Vec<bool> = algo
        .trim()
        .chars()
        .map(|c| match c {
            '.' => false,
            '#' => true,
            _ => unreachable!(),
        })
        .collect();

    let mut grid: Vec<Vec<bool>> = grid
        .trim()
        .lines()
        .map(|line| {
            line.trim()
                .chars()
                .map(|c| match c {
                    '.' => false,
                    '#' => true,
                    _ => unreachable!(),
                })
                .collect()
        })
        .collect();

    add_border(&mut grid, 2, false);

    let n_steps = match _part {
        Part::One => 2,
        Part::Two => 50,
    };
    for i in 0..n_steps {
        grid = enhance(&mut grid, &algo);

        switch_border(&mut grid, 1, algo[0] && i % 2 == 0);
        add_border(&mut grid, 1, algo[0] && i % 2 == 0);
    }

    let count = grid.iter().fold(0, |sum, row| {
        sum + row.iter().fold(0, |sum, el| sum + *el as u64)
    });

    count
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::fs;

    fn get_input() -> String {
        fs::read_to_string("data/day20/input-test.txt").expect("error")
    }

    #[test]
    fn part1_example() {
        assert_eq!(day20(get_input(), &Part::One), 35)
    }
    #[test]
    fn part2_example() {
        assert_eq!(day20(get_input(), &Part::Two), 3351)
    }
}
