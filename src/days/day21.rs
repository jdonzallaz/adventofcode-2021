use crate::Part;
use cached::proc_macro::cached;
use std::cmp::max;

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
struct DeterministicDice {
    n_roll: u16,
    current_side: u16,
}

impl DeterministicDice {
    fn new() -> DeterministicDice {
        DeterministicDice {
            n_roll: 0,
            current_side: 0,
        }
    }

    fn roll(&mut self) -> u16 {
        self.n_roll += 1;
        self.current_side += 1;
        if self.current_side > 100 {
            self.current_side = 1;
        }

        self.current_side
    }
}

#[cached]
fn play(player: usize, positions: [u16; 2], points: [u16; 2]) -> (usize, usize) {
    // check winner
    if points[0] >= 21 {
        return (1, 0);
    } else if points[1] >= 21 {
        return (0, 1);
    }

    let mut wins = (0, 0);

    for i in 1..=3 {
        for j in 1..=3 {
            for k in 1..=3 {
                let n_points = i + j + k;
                let mut positions = positions;
                let mut points = points;

                let new_pos = (positions[player] - 1 + n_points) % 10 + 1;
                positions[player] = new_pos;
                points[player] += new_pos;

                let new_wins = play((player + 1) % 2, positions, points);

                wins.0 += new_wins.0;
                wins.1 += new_wins.1;
            }
        }
    }

    wins
}

pub fn day21(_data: String, _part: &Part) -> u64 {
    // Read initial positions of players
    let mut positions: Vec<u16> = _data
        .trim()
        .lines()
        .map(|line| {
            line.trim()
                .split_whitespace()
                .last()
                .unwrap()
                .parse()
                .unwrap()
        })
        .collect();

    let mut points = [0, 0];
    let mut player = 0;

    match _part {
        Part::One => {
            let mut dice = DeterministicDice::new();

            'run: loop {
                let n_points: u16 = (0..3).map(|_| dice.roll()).sum();
                let new_pos = (positions[player] - 1 + n_points) % 10 + 1;
                positions[player] = new_pos;
                points[player] += new_pos;

                // Check end conditions
                for p in points {
                    if p >= 1000 {
                        break 'run;
                    }
                }

                // Next player
                player = (player + 1) % 2;
            }

            let score_looser = *points.iter().min().unwrap();
            let n_roll = dice.n_roll;

            score_looser as u64 * n_roll as u64
        }
        Part::Two => {
            let (win_p1, win_p2) = play(player, [positions[0], positions[1]], points);
            max(win_p1, win_p2) as u64
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::fs;

    fn get_input() -> String {
        fs::read_to_string("data/day21/input-test.txt").expect("error")
    }

    #[test]
    fn part1_example() {
        assert_eq!(day21(get_input(), &Part::One), 739785)
    }
    #[test]
    fn part2_example() {
        assert_eq!(day21(get_input(), &Part::Two), 444356092776315)
    }
}
