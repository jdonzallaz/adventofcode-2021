use crate::Part;
use regex::Regex;
use std::cmp::{max, min};
use std::collections::HashMap;

type Bounds = ((i64, i64), (i64, i64), (i64, i64));

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
enum Status {
    On = 1,
    Off = -1,
}

fn bound_size(bounds: Bounds) -> usize {
    (bounds.0 .0..=bounds.0 .1).count()
        * (bounds.1 .0..=bounds.1 .1).count()
        * (bounds.2 .0..=bounds.2 .1).count()
}

fn bound_size_under(bounds: Bounds, limit: i64) -> usize {
    (max(bounds.0 .0, -limit)..=min(bounds.0 .1, limit)).count()
        * (max(bounds.1 .0, -limit)..=min(bounds.1 .1, limit)).count()
        * (max(bounds.2 .0, -limit)..=min(bounds.2 .1, limit)).count()
}

fn intersection(b1: Bounds, b2: Bounds) -> Bounds {
    (
        (max(b1.0 .0, b2.0 .0), min(b1.0 .1, b2.0 .1)),
        (max(b1.1 .0, b2.1 .0), min(b1.1 .1, b2.1 .1)),
        (max(b1.2 .0, b2.2 .0), min(b1.2 .1, b2.2 .1)),
    )
}

fn valid_bounds(bounds: Bounds) -> bool {
    bounds.0 .0 <= bounds.0 .1 && bounds.1 .0 <= bounds.1 .1 && bounds.2 .0 <= bounds.2 .1
}

fn parse(data: &str) -> Vec<(Status, Bounds)> {
    data.trim()
        .lines()
        .map(|line| {
            let (status, bounds) = line.trim().split_once(" ").unwrap();
            let bounds = Regex::new(r"-?\d+")
                .unwrap()
                .find_iter(bounds)
                .map(|mat| mat.as_str())
                .map(|s| s.parse().unwrap())
                .collect::<Vec<i64>>();

            let status = match status {
                "on" => Status::On,
                "off" => Status::Off,
                _ => unreachable!(),
            };

            (
                status,
                (
                    (bounds[0], bounds[1]),
                    (bounds[2], bounds[3]),
                    (bounds[4], bounds[5]),
                ),
            )
        })
        .collect()
}

pub fn day22(_data: String, _part: &Part) -> u64 {
    let steps = parse(&_data);

    let mut cubes_status = HashMap::new();
    for (status, bounds) in steps {
        cubes_status
            .clone()
            .iter()
            .filter(|(_, v)| **v != 0)
            .map(|(k, v)| (intersection(bounds, *k), *v))
            .filter(|(k, _)| valid_bounds(*k))
            .for_each(|(k, v)| {
                *cubes_status.entry(k).or_insert(0) -= v;
            });

        if matches!(status, Status::On) {
            *cubes_status.entry(bounds).or_insert(0) += status as i64;
        }
    }

    match _part {
        Part::One => cubes_status
            .iter()
            .fold(0, |sum, (k, v)| sum + bound_size_under(*k, 50) as i64 * v)
            as u64,
        Part::Two => cubes_status
            .iter()
            .fold(0, |sum, (k, v)| sum + bound_size(*k) as i64 * v) as u64,
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::fs;

    fn get_input() -> String {
        fs::read_to_string("data/day22/input-test.txt").expect("error")
    }

    #[test]
    fn part1_example() {
        assert_eq!(day22(get_input(), &Part::One), 474140)
    }
    #[test]
    fn part2_example() {
        assert_eq!(day22(get_input(), &Part::Two), 2758514936282235)
    }
}
