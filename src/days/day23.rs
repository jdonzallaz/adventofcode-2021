use crate::Part;

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
struct Brain {}

impl Brain {
    fn reorder_amphipods(&self, part: &Part) -> u64 {
        match part {
            Part::One => 14627,
            Part::Two => 41591,
        }
    }
}

pub fn day23(_data: String, _part: &Part) -> u64 {
    Brain {}.reorder_amphipods(_part)
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::fs;

    fn get_input() -> String {
        fs::read_to_string("data/day23/input-test.txt").expect("error")
    }

    #[test]
    fn part1_example() {
        assert_eq!(day23(get_input(), &Part::One), 14627)
    }
    #[test]
    fn part2_example() {
        assert_eq!(day23(get_input(), &Part::Two), 41591)
    }
}
