use crate::Part;

fn run(program: Vec<Vec<String>>, input: Vec<i32>) -> [i32; 4] {
    let mut input_iter = input.into_iter();
    let mut registers = [0; 4];

    for instruction in program {
        let reg_id = match instruction[1].as_str() {
            "w" => 0,
            "x" => 1,
            "y" => 2,
            "z" => 3,
            _ => unreachable!(),
        };

        if matches!(instruction[0].as_str(), "inp") {
            registers[reg_id] = input_iter.next().unwrap();
            continue;
        }

        let val = match instruction[2].as_str() {
            "w" => registers[0],
            "x" => registers[1],
            "y" => registers[2],
            "z" => registers[3],
            d => d.parse().unwrap(),
        };

        registers[reg_id] = match instruction[0].as_str() {
            "add" => registers[reg_id] + val,
            "mul" => registers[reg_id] * val,
            "div" => registers[reg_id] / val,
            "mod" => registers[reg_id] % val,
            "eql" => (registers[reg_id] == val) as i32,
            _ => unreachable!(),
        };
    }

    registers
}

fn parse_program(data: String) -> Vec<Vec<String>> {
    data.trim()
        .lines()
        .map(|line| line.split_whitespace().map(|s| s.to_string()).collect())
        .collect()
}

fn parse_run(_data: String, input: Vec<i32>) -> [i32; 4] {
    run(parse_program(_data), input)
}

pub fn day24(_data: String, _part: &Part) -> u64 {
    let input = match _part {
        Part::One => vec![8, 9, 9, 1, 3, 9, 4, 9, 2, 9, 3, 9, 8, 9], // 89913949293989
        Part::Two => vec![1, 2, 9, 1, 1, 8, 1, 6, 1, 7, 1, 7, 1, 2], // 12911816171712
    };

    let res = parse_run(_data, input);
    eprintln!("res = {:?}", res);

    res[3] as u64
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::fs;

    fn get_input() -> String {
        fs::read_to_string("data/day24/input-test.txt").expect("error")
    }

    #[test]
    fn both_part_example() {
        assert_eq!(parse_run(get_input(), vec![13]), [1, 1, 0, 1])
    }
}
