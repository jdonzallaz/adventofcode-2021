use crate::Part;
use itertools::Itertools;

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
enum SeaCucumber {
    East,
    South,
    None,
}

fn _print(map: &[Vec<SeaCucumber>]) {
    println!(
        "{}",
        map.iter()
            .map(|row| row
                .iter()
                .map(|sc| match sc {
                    SeaCucumber::East => ">",
                    SeaCucumber::South => "v",
                    SeaCucumber::None => ".",
                })
                .join(""))
            .join("\n")
    )
}

fn move_herd(map: &mut Vec<Vec<SeaCucumber>>, kind: SeaCucumber) -> i32 {
    let mut n_move = 0;
    let map_copy = map.clone();

    for y in 0..map_copy.len() {
        for x in 0..map_copy[0].len() {
            let mut next_x = x;
            let mut next_y = y;

            match (map_copy[y][x], kind) {
                (SeaCucumber::East, SeaCucumber::East) => next_x = (x + 1) % map[0].len(),
                (SeaCucumber::South, SeaCucumber::South) => next_y = (y + 1) % map.len(),
                _ => continue,
            }

            if let SeaCucumber::None = map_copy[next_y][next_x] {
                map[next_y][next_x] = map_copy[y][x];
                map[y][x] = SeaCucumber::None;
                n_move += 1;
            }
        }
    }

    n_move
}

pub fn day25(_data: String, _part: &Part) -> u64 {
    let mut map: Vec<Vec<_>> = _data
        .trim()
        .lines()
        .map(|line| {
            line.trim()
                .chars()
                .map(|c| match c {
                    '>' => SeaCucumber::East,
                    'v' => SeaCucumber::South,
                    '.' => SeaCucumber::None,
                    _ => unreachable!(),
                })
                .collect()
        })
        .collect();

    let mut n_steps = 0;
    let mut n_move = 1;

    while n_move > 0 {
        n_move = 0;
        n_move += move_herd(&mut map, SeaCucumber::East);
        n_move += move_herd(&mut map, SeaCucumber::South);

        n_steps += 1;
    }

    n_steps
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::fs;

    fn get_input() -> String {
        fs::read_to_string("data/day25/input-test.txt").expect("error")
    }

    #[test]
    fn part1_example() {
        assert_eq!(day25(get_input(), &Part::One), 58)
    }
}
