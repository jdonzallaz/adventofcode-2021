use crate::parsing::parse_digit_grid;
use crate::Part;
use std::cmp::Ordering;

pub fn day3(data: String, _part: &Part) -> u64 {
    let len = data.lines().next().unwrap().len();

    if matches!(_part, Part::One) {
        let mut pos_count = vec![0; len];

        for line in data.lines() {
            for (i, c) in line.chars().enumerate() {
                pos_count[i] += match c {
                    '0' => -1,
                    '1' => 1,
                    _ => panic!("Not a binary"),
                };
            }
        }

        let mut gamma_str_bin = String::from("");
        let mut epsilon_str_bin = String::from("");
        for i in pos_count {
            match i.cmp(&0) {
                Ordering::Less => {
                    gamma_str_bin.push('0');
                    epsilon_str_bin.push('1');
                }
                Ordering::Greater => {
                    gamma_str_bin.push('1');
                    epsilon_str_bin.push('0');
                }
                Ordering::Equal => panic!("Error in input"),
            }
        }

        let gamma = u64::from_str_radix(&gamma_str_bin, 2).unwrap();
        let epsilon = u64::from_str_radix(&epsilon_str_bin, 2).unwrap();

        println!("Gamma: {}, {}", gamma_str_bin, gamma);
        println!("Epsilon: {}, {}", epsilon_str_bin, epsilon);

        return gamma * epsilon;
    }

    let mut systems = Vec::new();

    for system in 0..2 {
        let mut values = parse_digit_grid(&data);

        for i in 0..len {
            let mut count = 0;

            for line in &values {
                count += match line.get(i) {
                    Some(0) => -1,
                    Some(1) => 1,
                    _ => panic!("Not a binary"),
                };
            }

            let mut keep = match count.cmp(&0) {
                Ordering::Less => 0,
                Ordering::Greater => 1,
                Ordering::Equal => 1,
            };

            if system == 1 {
                keep = (keep + 1) % 2;
            }

            values.retain(|v| v[i] == keep);
            if values.len() == 1 {
                break;
            }
        }

        systems.push(
            values[0]
                .iter()
                .map(|n| n.to_string())
                .collect::<Vec<String>>()
                .join(""),
        );
    }

    let oxygen = u64::from_str_radix(&systems[0], 2).unwrap();
    let co2 = u64::from_str_radix(&systems[1], 2).unwrap();

    println!("Oxygen: {}", oxygen);
    println!("CO2: {}", co2);

    oxygen * co2
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::fs;

    fn get_input() -> String {
        fs::read_to_string("data/day3/input-test.txt").expect("error")
    }

    #[test]
    fn part1_example() {
        assert_eq!(day3(get_input(), &Part::One), 198)
    }
    #[test]
    fn part2_example() {
        assert_eq!(day3(get_input(), &Part::Two), 230)
    }
}
