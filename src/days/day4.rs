use crate::parsing::{parse_unsigned_vec, parse_whitespace_unsigned_vec};
use crate::Part;
use std::collections::HashSet;

pub fn day4(data: String, _part: &Part) -> u64 {
    let drawn_and_boards: Vec<&str> = data.split("\n\n").collect();

    let drawn = &drawn_and_boards[0];
    let drawn: Vec<u64> = parse_unsigned_vec(drawn, ",");

    let boards = &drawn_and_boards[1..];
    let mut boards: Vec<Vec<Vec<u64>>> = boards
        .iter()
        .map(|board| {
            board
                .trim()
                .lines()
                .map(parse_whitespace_unsigned_vec)
                .collect()
        })
        .collect();

    let mut winner_board = 0;
    let mut winner_number = 100;
    let mut winner_boards = HashSet::new();

    'outer: for d in drawn {
        for i_board in 0..boards.len() {
            for i_row in 0..boards[i_board].len() {
                for i_col in 0..boards[i_board][i_row].len() {
                    if boards[i_board][i_row][i_col] == d {
                        boards[i_board][i_row][i_col] = 100;

                        if boards[i_board].iter().map(|row| row[i_col]).sum::<u64>()
                            == 100 * boards[i_board].len() as u64
                            || boards[i_board][i_row].iter().sum::<u64>()
                                == 100 * boards[i_board][i_row].len() as u64
                        {
                            winner_board = i_board;
                            winner_number = d;

                            match _part {
                                Part::One => break 'outer,
                                Part::Two => {
                                    winner_boards.insert(winner_board);
                                    if winner_boards.len() == boards.len() {
                                        break 'outer;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    let mut sum = 0;
    for row in &boards[winner_board] {
        for col in row {
            if *col < 100 {
                sum += *col;
            }
        }
    }

    println!("sum={}, last nb={}", sum, winner_number);

    sum * winner_number
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::fs;

    fn get_input() -> String {
        fs::read_to_string("data/day4/input-test.txt").expect("error")
    }

    #[test]
    fn part1_example() {
        assert_eq!(day4(get_input(), &Part::One), 4512)
    }
    #[test]
    fn part2_example() {
        assert_eq!(day4(get_input(), &Part::Two), 1924)
    }
}
