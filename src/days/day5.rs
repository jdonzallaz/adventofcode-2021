use crate::Part;
use std::cmp::Ordering;
use std::collections::HashMap;
use std::fmt::Debug;
use std::str::FromStr;

#[derive(Hash, Eq, PartialEq, Debug, Clone)]
struct Point<T: Clone> {
    x: T,
    y: T,
}

impl<T> FromStr for Point<T>
where
    T: FromStr + Clone,
    <T as FromStr>::Err: Debug,
{
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (x, y) = s.trim().split_once(",").unwrap();
        Ok(Point {
            x: x.trim().parse::<T>().unwrap(),
            y: y.trim().parse::<T>().unwrap(),
        })
    }
}

#[derive(Debug)]
struct Segment<T: Clone> {
    p1: Point<T>,
    p2: Point<T>,
}

fn range(from: u64, to: u64) -> Vec<u64> {
    match from.cmp(&to) {
        Ordering::Less => (from..=to).collect(),
        Ordering::Equal => vec![from],
        Ordering::Greater => (to..=from).rev().collect(),
    }
}

impl Segment<u64> {
    pub fn points(&self) -> Vec<Point<u64>> {
        let mut points = Vec::new();
        let mut xs = range(self.p1.x, self.p2.x);
        let mut ys = range(self.p1.y, self.p2.y);

        match xs.len().cmp(&ys.len()) {
            Ordering::Less => xs = vec![xs[0]; ys.len()],
            Ordering::Equal => {}
            Ordering::Greater => ys = vec![ys[0]; xs.len()],
        }

        for it in xs.iter().zip(ys.iter_mut()) {
            let (x, y) = it;
            points.push(Point { x: *x, y: *y });
        }

        points
    }
}

impl<T> FromStr for Segment<T>
where
    T: FromStr + Clone,
    <T as FromStr>::Err: Debug,
{
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (p1, p2) = s.trim().split_once(" -> ").unwrap();

        Ok(Segment {
            p1: Point::from_str(p1).unwrap(),
            p2: Point::from_str(p2).unwrap(),
        })
    }
}

pub fn day5(_data: String, _part: &Part) -> u64 {
    let segments: Vec<Segment<u64>> = _data
        .lines()
        .map(|line| Segment::from_str(line).unwrap())
        .collect();

    let mut point_count: HashMap<Point<u64>, u64> = HashMap::new();
    for segment in segments {
        if matches!(_part, Part::Two)
            || segment.p1.x == segment.p2.x
            || segment.p1.y == segment.p2.y
        {
            let points = segment.points();

            for point in points {
                *point_count.entry(point).or_insert(0) += 1;
            }
        }
    }

    point_count.retain(|_, y| *y > 1);
    point_count.len() as u64
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::fs;

    fn get_input() -> String {
        fs::read_to_string("data/day5/input-test.txt").expect("error")
    }

    #[test]
    fn part1_example() {
        assert_eq!(day5(get_input(), &Part::One), 5)
    }
    #[test]
    fn part2_example() {
        assert_eq!(day5(get_input(), &Part::Two), 12)
    }
}
