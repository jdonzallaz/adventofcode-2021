use crate::parsing::parse_unsigned_vec;
use crate::Part;
use std::collections::HashMap;

pub fn day6(_data: String, _part: &Part) -> u64 {
    match _part {
        Part::One => run(_data, 80),
        Part::Two => run(_data, 256),
    }
}

fn run(_data: String, n_days: u32) -> u64 {
    let fishes: Vec<u8> = parse_unsigned_vec(&_data, ",");

    let mut map: HashMap<u8, u64> = HashMap::new();
    fishes.iter().for_each(|f| *map.entry(*f).or_insert(0) += 1);

    for _ in 0..n_days {
        map.clone().iter().for_each(|(&k, &v)| {
            if k == 0 {
                *map.entry(6).or_insert(0) += v;
                *map.entry(0).or_insert(0) -= v;
                *map.entry(8).or_insert(0) += v;
            } else {
                *map.entry(k - 1).or_insert(0) += v;
                *map.entry(k).or_insert(0) -= v;
            }
        });
    }

    map.values().sum()
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::fs;

    fn get_input() -> String {
        fs::read_to_string("data/day6/input-test.txt").expect("error")
    }

    #[test]
    fn part1_example() {
        assert_eq!(run(get_input(), 18), 26);
        assert_eq!(run(get_input(), 80), 5934);
    }
    #[test]
    fn part2_example() {
        assert_eq!(run(get_input(), 256), 26984457539);
    }
}
