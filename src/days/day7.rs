use crate::parsing::parse_unsigned_vec;
use crate::Part;
use std::cmp::min;

fn median(numbers: &[u64]) -> u64 {
    // Find the median, return the first of two middle numbers if length is even.
    numbers[numbers.len() / 2]
}
fn mean_ceil(numbers: &[u64]) -> u64 {
    (numbers.iter().sum::<u64>() as f32 / numbers.len() as f32).ceil() as u64
}
fn mean_floor(numbers: &[u64]) -> u64 {
    (numbers.iter().sum::<u64>() as f32 / numbers.len() as f32).floor() as u64
}

fn diff(n1: u64, n2: u64) -> u64 {
    if n1 > n2 {
        n1 - n2
    } else {
        n2 - n1
    }
}

fn partial_sum(n: u64) -> u64 {
    n * (n + 1) / 2
}

fn cost1(numbers: &[u64], position: u64) -> u64 {
    numbers.iter().fold(0, |sum, &n| sum + diff(n, position))
}
fn cost2(numbers: &[u64], position: u64) -> u64 {
    numbers
        .iter()
        .fold(0, |sum, &n| sum + partial_sum(diff(n, position)))
}

pub fn day7(_data: String, _part: &Part) -> u64 {
    let mut numbers = parse_unsigned_vec(&_data, ",");

    numbers.sort_unstable();

    let total_cost = match _part {
        Part::One => {
            let position = median(&numbers);
            eprintln!("position = {:?}", position);
            cost1(&numbers, position)
        }
        Part::Two => {
            let position1 = mean_floor(&numbers);
            eprintln!("position1 = {:?}", position1);
            let c1 = cost2(&numbers, position1);
            let position2 = mean_ceil(&numbers);
            eprintln!("position2 = {:?}", position2);
            let c2 = cost2(&numbers, position2);
            min(c1, c2)
        }
    };

    eprintln!("total_cost = {:?}", total_cost);

    total_cost
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::fs;

    fn get_input() -> String {
        fs::read_to_string("data/day7/input-test.txt").expect("error")
    }

    #[test]
    fn part1_example() {
        assert_eq!(day7(get_input(), &Part::One), 37)
    }
    #[test]
    fn part2_example() {
        assert_eq!(day7(get_input(), &Part::Two), 168)
    }
}
