use crate::Part;
use std::collections::HashMap;

fn parse_pattern(pattern_list: &str) -> Vec<Vec<char>> {
    pattern_list
        .trim()
        .split_whitespace()
        .map(|s| {
            let mut c: Vec<char> = s.chars().collect();
            c.sort_unstable();
            c
        })
        .collect()
}

pub fn day8(_data: String, _part: &Part) -> u64 {
    let seg_to_nb: HashMap<&str, u8> = [
        ("abcefg", 0),
        ("cf", 1),
        ("acdeg", 2),
        ("acdfg", 3),
        ("bcdf", 4),
        ("abdfg", 5),
        ("abdefg", 6),
        ("acf", 7),
        ("abcdefg", 8),
        ("abcdfg", 9),
    ]
    .into_iter()
    .collect();

    let mut count = 0;

    for line in _data.trim().lines() {
        let (signals, segments) = line.trim().split_once('|').unwrap();
        let mut signals = parse_pattern(signals);
        signals.sort_unstable_by_key(|a| a.len());
        let segments = parse_pattern(segments);

        let signal_1 = signals[0].clone();
        let signal_7 = signals[1].clone();
        let signal_4 = signals[2].clone();
        let signal_8 = signals[9].clone();
        let signals_069 = &signals[6..9];
        let signal_6 = signals_069
            .iter()
            .find(|sig| !sig.contains(&signal_1[0]) || !sig.contains(&signal_1[1]))
            .unwrap()
            .clone();

        let mut sig_to_seg = HashMap::new();

        // Find signal for c,f (in 1)
        if signal_6.contains(&signal_1[0]) {
            sig_to_seg.insert(signal_1[0], 'f');
            sig_to_seg.insert(signal_1[1], 'c');
        } else {
            sig_to_seg.insert(signal_1[0], 'c');
            sig_to_seg.insert(signal_1[1], 'f');
        }

        // Find signal for a (in 7)
        let sig_a = signal_7.iter().find(|seg| !signal_1.contains(seg)).unwrap();
        sig_to_seg.insert(*sig_a, 'a');

        // Find signal for b,d (in 4)
        let sig_bd: Vec<&char> = signal_4
            .iter()
            .filter(|seg| !signal_1.contains(seg))
            .collect();
        let signal_0 = signals_069
            .iter()
            .find(|sig| !sig.contains(sig_bd[0]) || !sig.contains(sig_bd[1]))
            .unwrap()
            .clone();
        if signal_0.contains(sig_bd[0]) {
            sig_to_seg.insert(*sig_bd[0], 'b');
            sig_to_seg.insert(*sig_bd[1], 'd');
        } else {
            sig_to_seg.insert(*sig_bd[0], 'd');
            sig_to_seg.insert(*sig_bd[1], 'b');
        }

        // Find signal for e,g
        let signal_9 = signals_069
            .iter()
            .find(|sig| **sig != signal_0 && **sig != signal_6)
            .unwrap()
            .clone();
        let sig_e = signal_8.iter().find(|sig| !signal_9.contains(sig)).unwrap();
        sig_to_seg.insert(*sig_e, 'e');
        let sig_g = signal_8
            .iter()
            .find(|sig| !sig_to_seg.contains_key(sig))
            .unwrap();
        sig_to_seg.insert(*sig_g, 'g');

        // ----- Here we have the complete map

        let numbers: Vec<u8> = segments
            .into_iter()
            .map(|segment| {
                segment
                    .into_iter()
                    .map(|c| *sig_to_seg.get(&c).unwrap())
                    .collect::<Vec<char>>()
            })
            .map(|mut segment| {
                segment.sort_unstable();
                segment
            })
            .map(|segment| segment.into_iter().collect::<String>())
            .map(|segment| *seg_to_nb.get(segment.as_str()).unwrap())
            .collect();

        count += match _part {
            Part::One => numbers
                .into_iter()
                .filter(|n| n == &1 || n == &4 || n == &7 || n == &8)
                .count() as u64,
            Part::Two => numbers
                .into_iter()
                .map(|n| n.to_string())
                .collect::<Vec<String>>()
                .join("")
                .parse()
                .unwrap(),
        }
    }

    count
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::fs;

    fn get_input() -> String {
        fs::read_to_string("data/day8/input-test.txt").expect("error")
    }

    #[test]
    fn part1_example() {
        assert_eq!(day8(get_input(), &Part::One), 26)
    }
    #[test]
    fn part2_example() {
        assert_eq!(day8(get_input(), &Part::Two), 61229)
    }
}
