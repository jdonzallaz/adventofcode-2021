use crate::parsing::parse_digit_grid;
use crate::Part;
use std::collections::HashMap;

struct Basin {
    segments: HashMap<usize, Vec<(usize, usize)>>,
}

impl Basin {
    fn contains(&self, y: usize, from: usize, to: usize) -> bool {
        if y == 0 || !self.segments.contains_key(&(y - 1)) {
            return false;
        }

        for segment in self.segments.get(&(y - 1)).unwrap() {
            if from <= segment.1 && to >= segment.0 {
                return true;
            }
        }

        false
    }

    fn add(&mut self, y: usize, from: usize, to: usize) {
        (*self.segments.entry(y).or_insert_with(Vec::new)).push((from, to));
    }
}

fn find_basin(basins: &mut Vec<Basin>, y: usize, from: usize, to: usize) {
    let mut found_in = Vec::new();

    for (i, basin) in basins.iter_mut().enumerate() {
        if basin.contains(y, from, to) {
            found_in.push(i);
        }
    }

    let mut basin = Basin {
        segments: HashMap::new(),
    };
    basin.add(y, from, to);

    for i in found_in.iter().rev() {
        for (k, v) in basins[*i].segments.iter() {
            for segment in v {
                basin.add(*k, segment.0, segment.1);
            }
        }
        basins.remove(*i);
    }

    basins.push(basin);
}

pub fn day9(_data: String, _part: &Part) -> u64 {
    let grid = parse_digit_grid(&_data);

    if matches!(_part, Part::One) {
        let mut risk_level_sum = 0;
        for y in 0..grid.len() {
            for x in 0..grid[0].len() {
                let el = grid[y][x];
                if (y == 0 || grid[y - 1][x] > el)
                    && (y == grid.len() - 1 || grid[y + 1][x] > el)
                    && (x == 0 || grid[y][x - 1] > el)
                    && (x == grid[0].len() - 1 || grid[y][x + 1] > el)
                {
                    risk_level_sum += 1 + grid[y][x] as u64;
                }
            }
        }

        risk_level_sum
    } else {
        let mut basins = Vec::new();

        for y in 0..grid.len() {
            let mut start = 0; // start of segment
            let mut end = 0; // end of segment (included)
            let mut started = false;
            for x in 0..grid[0].len() {
                let el = grid[y][x];
                if el == 9 {
                    if started && end >= start {
                        find_basin(&mut basins, y, start, end);
                    }
                    start = x + 1;
                } else {
                    end = x;
                    started = true;
                }
            }
            if started && end >= start {
                find_basin(&mut basins, y, start, end);
            }
        }

        let mut sizes: Vec<usize> = basins
            .iter()
            .map(|basin| {
                basin
                    .segments
                    .values()
                    .map(|segments| {
                        segments
                            .iter()
                            .map(|(from, to)| to - from + 1)
                            .sum::<usize>()
                    })
                    .sum::<usize>()
            })
            .collect();

        sizes.sort_unstable();
        sizes.iter().rev().take(3).product::<usize>() as u64
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::fs;

    fn get_input() -> String {
        fs::read_to_string("data/day9/input-test.txt").expect("error")
    }

    #[test]
    fn part1_example() {
        assert_eq!(day9(get_input(), &Part::One), 15)
    }
    #[test]
    fn part2_example() {
        assert_eq!(day9(get_input(), &Part::Two), 1134)
    }
}
