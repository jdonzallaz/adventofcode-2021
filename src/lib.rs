pub mod days;
mod parsing;

use argh::FromArgs;

pub enum Part {
    One,
    Two,
}

#[derive(Debug, FromArgs)]
/// Advent of Code 2021
pub struct Config {
    #[argh(option, short = 'd', default = "1")]
    /// which day
    pub day: u8,

    #[argh(option, short = 'p', default = "1")]
    /// which part
    pub part: u8,

    #[argh(switch, short = 't')]
    /// in test mode ?
    pub test: bool,
}
