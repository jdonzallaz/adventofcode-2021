use aoc::days;
use aoc::{Config, Part};
use std::{fs, process};

fn main() {
    let config: Config = argh::from_env();
    let part: Part = match config.part {
        1 => Part::One,
        2 => Part::Two,
        _ => panic!("Wrong part"),
    };

    let test_str = if config.test { "-test" } else { "" };
    let data_path = format!("data/day{}/input{}.txt", config.day, test_str);
    let contents = fs::read_to_string(data_path).expect("error");

    let result = match config.day {
        1 => days::day1::day1(contents, &part),
        2 => days::day2::day2(contents, &part),
        3 => days::day3::day3(contents, &part),
        4 => days::day4::day4(contents, &part),
        5 => days::day5::day5(contents, &part),
        6 => days::day6::day6(contents, &part),
        7 => days::day7::day7(contents, &part),
        8 => days::day8::day8(contents, &part),
        9 => days::day9::day9(contents, &part),
        10 => days::day10::day10(contents, &part),
        11 => days::day11::day11(contents, &part),
        12 => days::day12::day12(contents, &part),
        13 => days::day13::day13(contents, &part),
        14 => days::day14::day14(contents, &part),
        15 => days::day15::day15(contents, &part),
        16 => days::day16::day16(contents, &part),
        17 => days::day17::day17(contents, &part),
        18 => days::day18::day18(contents, &part),
        19 => days::day19::day19(contents, &part),
        20 => days::day20::day20(contents, &part),
        21 => days::day21::day21(contents, &part),
        22 => days::day22::day22(contents, &part),
        23 => days::day23::day23(contents, &part),
        24 => days::day24::day24(contents, &part),
        25 => days::day25::day25(contents, &part),
        other_day => {
            eprintln!("Day {} is not implemented yet.", other_day);
            process::exit(1);
        }
    };

    println!("Result: {}", result);
}
