use num::Unsigned;
use std::str::FromStr;

pub fn parse_unsigned_vec<T: Unsigned + FromStr>(data: &str, pattern: &str) -> Vec<T> {
    data.trim()
        .split(pattern)
        .map(|s| s.trim().parse().ok().unwrap())
        .collect()
}

pub fn parse_whitespace_unsigned_vec<T: Unsigned + FromStr>(data: &str) -> Vec<T> {
    data.trim()
        .split_whitespace()
        .map(|s| s.trim().parse().ok().unwrap())
        .collect()
}

pub fn parse_lines_unsigned_vec<T: Unsigned + FromStr>(data: &str) -> Vec<T> {
    data.trim()
        .lines()
        .map(|line| line.trim().parse().ok().unwrap())
        .collect()
}

pub fn parse_digit_grid(data: &str) -> Vec<Vec<u8>> {
    data.trim()
        .lines()
        .map(|line| {
            line.trim()
                .chars()
                .map(|c| c.to_string().parse().unwrap())
                .collect()
        })
        .collect()
}
